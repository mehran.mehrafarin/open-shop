-- MySQL dump 10.13  Distrib 5.7.32, for osx10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: open_shop
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `x_category`
--

DROP TABLE IF EXISTS `x_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_category`
--

LOCK TABLES `x_category` WRITE;
/*!40000 ALTER TABLE `x_category` DISABLE KEYS */;
INSERT INTO `x_category` VALUES (1,'0ae53188-8012-47c8-ab05-d6a7075c6686','laptop','This is laptop category',NULL);
/*!40000 ALTER TABLE `x_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_comment`
--

DROP TABLE IF EXISTS `x_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `product` (`product`),
  CONSTRAINT `x_comment_ibfk_product` FOREIGN KEY (`product`) REFERENCES `x_product` (`id`),
  CONSTRAINT `x_comment_ibfk_user` FOREIGN KEY (`user`) REFERENCES `x_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_comment`
--

LOCK TABLES `x_comment` WRITE;
/*!40000 ALTER TABLE `x_comment` DISABLE KEYS */;
INSERT INTO `x_comment` VALUES (1,2,1,'0ae53188-8012-47c8-ab05-d6a7075c6686','John says:','My name is John','enable'),(2,3,1,'907f813b-c99c-4fb5-b4a1-e4ea39e5dabf','Sara says:','My name is Sara','enable'),(3,4,1,'5e6fa947-8e40-4855-bb15-2e9fe5fc9b5f','Alex says:','My name is Alex','enable'),(4,5,1,'053e3a6a-b708-4a60-bb46-cc92c5537947','Jean says:','My name is Jean','enable'),(5,6,1,'00056ece-32bd-44fd-9a45-c9167d2201ea','James says:','My name is James','enable'),(6,7,2,'882f58fb-294f-43f0-b19b-d3764504aa78','Robert says:','My name is Robert','enable'),(7,8,2,'f9e6f347-8e16-41a4-bc2a-b9d29e750c69','Mary says:','My name is Mary','enable'),(8,9,2,'d4365881-6dfc-4e70-8602-d9a2ec0d5602','Patricia says:','My name is Patricia','enable'),(9,10,2,'c820537f-fefc-40d9-8b00-c8f40d496a10','Jennifer says:','My name is Jennifer','enable'),(10,11,2,'7a005eb3-7f14-449b-a581-eed6be57a106','David says:','My name is David','enable'),(11,12,3,'9b435f97-362b-4c13-af78-2736ec5c3a86','Richard says:','My name is Richard','enable'),(12,13,3,'e50f313c-a943-4630-9641-5fbedad286a0','Joseph says:','My name is Joseph','enable'),(13,14,3,'d58844de-7853-4895-942d-93c248cc9bbc','Mark says:','My name is Mark','enable'),(14,15,3,'575682eb-9f40-4a2b-a927-df14e147c837','Sharon says:','My name is Sharon','enable'),(15,16,3,'5742b57e-7163-4509-ba5d-5d273ed2a680','Laura says:','My name is Laura','enable'),(16,17,4,'bea2d2e4-ee90-42ce-939e-e7810264f8fb','Amy says:','My name is Amy','enable'),(17,18,4,'021a5f42-be03-46ba-800f-687744c9ee8e','Shirley says:','My name is Shirley','enable'),(18,19,4,'fc598c43-a753-44fb-be72-957954d3bb33','Angela says:','My name is Angela','enable'),(19,20,4,'57338742-a3c8-44ee-8c90-e83a5343ec3b','Helen says:','My name is Helen','enable'),(20,21,4,'8ed03c09-f884-4c01-8a7d-eeed30eedb2f','Brenda says:','My name is Brenda','enable'),(21,22,5,'a5284f13-6ebb-4c4a-b6fe-1d8e2f8c1627','Nicole says:','My name is Nicole','enable'),(22,23,5,'b314bc42-5b70-4c28-a01a-54efa23906b3','Katherine says:','My name is Katherine','enable'),(23,24,5,'ff708adc-d9c4-4284-90ed-a22f3336eda2','Tyler says:','My name is Tyler','enable'),(24,25,5,'44a1c193-b237-4feb-a6f0-eaed553282fe','Samuel says:','My name is Samuel','enable'),(25,26,5,'400dbcf1-f318-434b-8e34-f241acb44750','Frank says:','My name is Frank','enable');
/*!40000 ALTER TABLE `x_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_product`
--

DROP TABLE IF EXISTS `x_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `price` bigint(20) unsigned DEFAULT NULL,
  `rate` smallint(5) unsigned DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  CONSTRAINT `x_product_ibfk_category` FOREIGN KEY (`category`) REFERENCES `x_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_product`
--

LOCK TABLES `x_product` WRITE;
/*!40000 ALTER TABLE `x_product` DISABLE KEYS */;
INSERT INTO `x_product` VALUES (1,1,'0ae53188-8012-47c8-ab05-d6a7075c6686','Asus Laptop',1000,NULL,'enable'),(2,1,'907f813b-c99c-4fb5-b4a1-e4ea39e5dabf','MIS Laptop',900,NULL,'enable'),(3,1,'5e6fa947-8e40-4855-bb15-2e9fe5fc9b5f','Dell Laptop',1200,NULL,'enable'),(4,1,'053e3a6a-b708-4a60-bb46-cc92c5537947','Lenovo Laptop',700,NULL,'enable'),(5,1,'00056ece-32bd-44fd-9a45-c9167d2201ea','HP Laptop',2500,NULL,'enable'),(6,1,'882f58fb-294f-43f0-b19b-d3764504aa78','Apple Laptop',1400,NULL,'enable');
/*!40000 ALTER TABLE `x_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_rate`
--

DROP TABLE IF EXISTS `x_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_rate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `value` smallint(6) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `product` (`product`),
  CONSTRAINT `x_rate_ibfk_product` FOREIGN KEY (`product`) REFERENCES `x_product` (`id`),
  CONSTRAINT `x_rate_ibfk_user` FOREIGN KEY (`user`) REFERENCES `x_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_rate`
--

LOCK TABLES `x_rate` WRITE;
/*!40000 ALTER TABLE `x_rate` DISABLE KEYS */;
INSERT INTO `x_rate` VALUES (1,2,1,'0ae53188-8012-47c8-ab05-d6a7075c6686',4,'enable'),(2,3,1,'907f813b-c99c-4fb5-b4a1-e4ea39e5dabf',3,'enable'),(3,4,1,'5e6fa947-8e40-4855-bb15-2e9fe5fc9b5f',5,'enable'),(4,5,1,'053e3a6a-b708-4a60-bb46-cc92c5537947',1,'enable'),(5,6,1,'00056ece-32bd-44fd-9a45-c9167d2201ea',5,'enable'),(6,7,2,'882f58fb-294f-43f0-b19b-d3764504aa78',5,'enable'),(7,8,2,'f9e6f347-8e16-41a4-bc2a-b9d29e750c69',4,'enable'),(8,9,2,'d4365881-6dfc-4e70-8602-d9a2ec0d5602',5,'enable'),(9,10,2,'c820537f-fefc-40d9-8b00-c8f40d496a10',5,'enable'),(10,11,2,'7a005eb3-7f14-449b-a581-eed6be57a106',4,'enable'),(11,12,3,'9b435f97-362b-4c13-af78-2736ec5c3a86',3,'enable'),(12,13,3,'e50f313c-a943-4630-9641-5fbedad286a0',2,'enable'),(13,14,3,'d58844de-7853-4895-942d-93c248cc9bbc',1,'enable'),(14,15,3,'575682eb-9f40-4a2b-a927-df14e147c837',2,'enable'),(15,16,3,'5742b57e-7163-4509-ba5d-5d273ed2a680',2,'enable'),(16,17,4,'bea2d2e4-ee90-42ce-939e-e7810264f8fb',3,'enable'),(17,18,4,'021a5f42-be03-46ba-800f-687744c9ee8e',4,'enable'),(18,19,4,'fc598c43-a753-44fb-be72-957954d3bb33',5,'enable'),(19,20,4,'57338742-a3c8-44ee-8c90-e83a5343ec3b',4,'enable'),(20,21,4,'8ed03c09-f884-4c01-8a7d-eeed30eedb2f',3,'enable'),(21,22,5,'a5284f13-6ebb-4c4a-b6fe-1d8e2f8c1627',5,'enable'),(22,23,5,'b314bc42-5b70-4c28-a01a-54efa23906b3',4,'enable'),(23,24,5,'ff708adc-d9c4-4284-90ed-a22f3336eda2',4,'enable'),(24,25,5,'44a1c193-b237-4feb-a6f0-eaed553282fe',4,'enable'),(25,26,5,'400dbcf1-f318-434b-8e34-f241acb44750',3,'enable');
/*!40000 ALTER TABLE `x_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_role`
--

DROP TABLE IF EXISTS `x_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `edited_date` date DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_role`
--

LOCK TABLES `x_role` WRITE;
/*!40000 ALTER TABLE `x_role` DISABLE KEYS */;
INSERT INTO `x_role` VALUES (1,'0ae53188-8012-47c8-ab05-d6a7075c6686','ROLE_ADMIN','This is ADMIN role.','2021-09-17',NULL,'enable'),(2,'907f813b-c99c-4fb5-b4a1-e4ea39e5dabf','ROLE_USER','This is USER role.','2021-09-17',NULL,'enable');
/*!40000 ALTER TABLE `x_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_role_user`
--

DROP TABLE IF EXISTS `x_role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_role_user` (
  `role` bigint(20) NOT NULL,
  `user` bigint(20) NOT NULL,
  KEY `role` (`role`),
  KEY `user` (`user`),
  CONSTRAINT `x_role_user_ibfk_role` FOREIGN KEY (`role`) REFERENCES `x_role` (`id`),
  CONSTRAINT `x_role_user_ibfk_user` FOREIGN KEY (`user`) REFERENCES `x_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_role_user`
--

LOCK TABLES `x_role_user` WRITE;
/*!40000 ALTER TABLE `x_role_user` DISABLE KEYS */;
INSERT INTO `x_role_user` VALUES (1,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),(2,11),(2,12),(2,13),(2,14),(2,15),(2,16);
/*!40000 ALTER TABLE `x_role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `x_user`
--

DROP TABLE IF EXISTS `x_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `x_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `edited_date` date DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `x_user`
--

LOCK TABLES `x_user` WRITE;
/*!40000 ALTER TABLE `x_user` DISABLE KEYS */;
INSERT INTO `x_user` VALUES (1,'cbcce6e7-3542-4250-b1be-c5b7f35c6e60','Mehran','admin','$2a$10$aF.PYZpkGM824Ngu28k/Aua90AkJyo1u45GVOhV8VdHKcf0qFo8Iq','2021-09-17',NULL,'enable'),(2,'0ae53188-8012-47c8-ab05-d6a7075c6686','John','john','$2a$10$D2JYaRhBFxJoqvL.laCkAeOUL3UxHgi9pfhTN1Ygb5nTrPSAvPMZu','2021-09-17',NULL,'enable'),(3,'907f813b-c99c-4fb5-b4a1-e4ea39e5dabf','Sara','sara','$2a$10$lNvoTsMsKk2hFvhEMn2EnuvIirEvgNFsR1/qOQuBVjIGBcMWCpxpu','2021-09-17',NULL,'enable'),(4,'5e6fa947-8e40-4855-bb15-2e9fe5fc9b5f','Alex','alex','$2a$10$CqqlvDDbUSYCewRGNKTiseLeN0cqSkPBTgW.79GJiLjUsayekmQ62','2021-09-17',NULL,'enable'),(5,'053e3a6a-b708-4a60-bb46-cc92c5537947','Jean','jean','$2a$10$4es28lr7l0Wpu2XHHFjbyeGmzzv4Smn3V28eZjJyh3ipQMABcMfyq','2021-09-17',NULL,'enable'),(6,'00056ece-32bd-44fd-9a45-c9167d2201ea','James','james','$2a$10$84gM/Wj6y4v/a8JeYK.sUeon23T8rBiroiEuSTCsnRdjLJ6A2SWkq','2021-09-17',NULL,'enable'),(7,'882f58fb-294f-43f0-b19b-d3764504aa78','Robert','robert','$2a$10$FS.qTWYk5izWEbmzddBEcOq1wd1ehi9WU3WlWVb9XXa16sax6FYXy','2021-09-17',NULL,'enable'),(8,'f9e6f347-8e16-41a4-bc2a-b9d29e750c69','Mary','mary','$2a$10$IYrZJTh35RWvbDQZSVZjluuMv6V3KLzhzVd17Arw8HyODCnjirAEK','2021-09-17',NULL,'enable'),(9,'d4365881-6dfc-4e70-8602-d9a2ec0d5602','Patricia','patricia','$2a$10$c5ihPAyIHtyFyA2Hio0XS.vN1h6hCnj0XkeTbaKx.VhAHUdeDAiaS','2021-09-17',NULL,'enable'),(10,'c820537f-fefc-40d9-8b00-c8f40d496a10','Jennifer','jennifer','$2a$10$XucCIZrqz3s8Au0u2gwQpO5dwYPshxaqQCOpyEjPTBGk2MmV0avlK','2021-09-17',NULL,'enable'),(11,'7a005eb3-7f14-449b-a581-eed6be57a106','David','david','$2a$10$66/t/1lUGuLPhULh2dx5MunGUpPTxp2RJfk1uQAJ3rwkxSfmjxbMO','2021-09-17',NULL,'enable'),(12,'9b435f97-362b-4c13-af78-2736ec5c3a86','Richard','richard','$2a$10$BkmJO4v0O5Ulo9f/g9yIRufLFaUqSRbEySXICj.rRz1b3Hhsb4Uwm','2021-09-17',NULL,'enable'),(13,'e50f313c-a943-4630-9641-5fbedad286a0','Joseph','joseph','$2a$10$/y1/QNd9eZxGa.cy39PW3eupiI2o6VkPURxmYC3Sg1LPUTwEgRNey','2021-09-17',NULL,'enable'),(14,'d58844de-7853-4895-942d-93c248cc9bbc','Mark','mark','$2a$10$e3pn7AEufjqlKiBTJOM3V.7qrwIm65XIwojKHqD/9i98vWgovNGJa','2021-09-17',NULL,'enable'),(15,'575682eb-9f40-4a2b-a927-df14e147c837','Sharon','sharon','$2a$10$XQRufxYQxu2A7c2MJtuPde2pvegrXi79c5hxrq4mMsF69siAW63E6','2021-09-17',NULL,'enable'),(16,'5742b57e-7163-4509-ba5d-5d273ed2a680','Laura','laura','$2a$10$XAfTOaQN/G4FIUts/5lX7OczPC2DRvS8hNXQN7O3Pc3kSiPftqXB6','2021-09-17',NULL,'enable'),(17,'bea2d2e4-ee90-42ce-939e-e7810264f8fb','Amy','amy','$2a$10$rMtlhWxdOkGBiW4.fOZXz.mkB1dkREXGWmcWbW0I19QKFYVLmgvgm','2021-09-17',NULL,'enable'),(18,'021a5f42-be03-46ba-800f-687744c9ee8e','Shirley','shirley','$2a$10$bE84oxtfulc.PI4gnSp5..8nGiBiO34BrRnO9Ir5syaBm2Yb7f8D6','2021-09-17',NULL,'enable'),(19,'fc598c43-a753-44fb-be72-957954d3bb33','Angela','angela','$2a$10$C6Rzi6lweAxgRqdcKjfYNOk07LPeMZNtBjQfa2j6JjcPxFthgQvGK','2021-09-17',NULL,'enable'),(20,'57338742-a3c8-44ee-8c90-e83a5343ec3b','Helen','helen','$2a$10$zAFi6GLEsYwSBvCezq4DzOA6WNDAJRZBxzCTuoHDlSuCIPX4bBFAO','2021-09-17',NULL,'enable'),(21,'8ed03c09-f884-4c01-8a7d-eeed30eedb2f','Brenda','brenda','$2a$10$gNE5Ga7Cp26b4Af288dLiesV.Y1/PpkoQFzPfaeZNlEOlfxredXtW','2021-09-17',NULL,'enable'),(22,'a5284f13-6ebb-4c4a-b6fe-1d8e2f8c1627','Nicole','nicole','$2a$10$LZrs9RhSzBbLISRIJ3BRkunJin2S68ZhNNEoBNdrdhb6WTFbgs0lK','2021-09-17',NULL,'enable'),(23,'b314bc42-5b70-4c28-a01a-54efa23906b3','Katherine','katherine','$2a$10$7nCSrFijm8hkOfd8XB3/iOwg.Uw01BHhQbnLAwX3IJqc05P9Nxvma','2021-09-17',NULL,'enable'),(24,'ff708adc-d9c4-4284-90ed-a22f3336eda2','Tyler','tyler','$2a$10$pgc9bBAB/FgxMpMhF97LluOdXwGXqnsfgGlZsfDvdDMyiTQB354lO','2021-09-17',NULL,'enable'),(25,'44a1c193-b237-4feb-a6f0-eaed553282fe','Samuel','samuel','$2a$10$832KMglTtKJ9BSWMQ5S5teybpanK48Zoeg/UdQcQliJcmIlmSVMsq','2021-09-17',NULL,'enable'),(26,'400dbcf1-f318-434b-8e34-f241acb44750','Frank','frank','$2a$10$IuvDz33gkS2YHcoGbd55jOSw5jw9SVMRf6qtW7KidaAL8XIUTDM.q','2021-09-17',NULL,'enable');
/*!40000 ALTER TABLE `x_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-18  0:07:24
