DROP TABLE IF EXISTS x_role_user;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE x_role_user
(
    role BIGINT NOT NULL,
    user BIGINT NOT NULL,
    KEY role (role),
    KEY user (user),
    CONSTRAINT x_role_user_ibfk_user FOREIGN KEY (user) REFERENCES x_user (id),
    CONSTRAINT x_role_user_ibfk_role FOREIGN KEY (role) REFERENCES x_role (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

