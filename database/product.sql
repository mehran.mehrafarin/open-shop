DROP TABLE IF EXISTS x_product;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE x_product
(
    id       BIGINT  NOT NULL AUTO_INCREMENT,
    category BIGINT            DEFAULT NULL,
    code     VARCHAR(50)       DEFAULT NULL,
    name     VARCHAR(50)       DEFAULT NULL,
    price    BIGINT UNSIGNED   DEFAULT NULL,
    rate     SMALLINT UNSIGNED DEFAULT NULL,
    state    VARCHAR(10)       DEFAULT NULL,
    PRIMARY KEY (id),
    KEY category (category),
    CONSTRAINT x_product_ibfk_category foreign key (category) references x_category (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
