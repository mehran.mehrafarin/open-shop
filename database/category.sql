DROP TABLE IF EXISTS x_category;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE x_category
(
    id          BIGINT NOT NULL AUTO_INCREMENT,
    code       VARCHAR(50)  DEFAULT NULL,
    name       VARCHAR(50)  DEFAULT NULL,
    description VARCHAR(255) DEFAULT NULL,
    state       VARCHAR(10)  DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
