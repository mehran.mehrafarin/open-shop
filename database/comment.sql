DROP TABLE IF EXISTS x_comment;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE x_comment
(
    id          BIGINT  NOT NULL AUTO_INCREMENT,
    user        BIGINT       DEFAULT NULL,
    product     BIGINT       DEFAULT NULL,
    code        VARCHAR(50)  DEFAULT NULL,
    title       VARCHAR(50)  DEFAULT NULL,
    description VARCHAR(255) DEFAULT NULL,
    state       VARCHAR(10)  DEFAULT NULL,
    PRIMARY KEY (id),
    KEY user (user),
    KEY product (product),
    CONSTRAINT x_comment_ibfk_user foreign key (user) references x_user (id),
    CONSTRAINT x_comment_ibfk_product foreign key (product) references x_product (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
