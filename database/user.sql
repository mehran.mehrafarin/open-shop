DROP TABLE IF EXISTS x_user;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE x_user
(
    id           BIGINT  NOT NULL AUTO_INCREMENT,
    code         VARCHAR(50) DEFAULT NULL,
    name         VARCHAR(50) DEFAULT NULL,
    username     VARCHAR(50) DEFAULT NULL,
    password     VARCHAR(255) DEFAULT NULL,
    created_date DATE        DEFAULT NULL,
    edited_date  DATE        DEFAULT NULL,
    state        VARCHAR(10) DEFAULT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
