# open-shop - spring boot

This spring boot application is simple shopping application.

## Prerequisites

### JDK 8
Install JDK 8 from  [Oracle](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html "Download JDK")

### Maven
Install Maven from [Maven](https://maven.apache.org/install.html "Download Maven")

### Docker (optional)
Install Maven from [Docker](https://docs.docker.com/get-docker/ "Download Docker")


## Installation and running
### Without Docker
run blow commands in order:

```git clone https://gitlab.com/mehran.mehrafarin/open-shop.git```
```cd open-shop```
```mvn spring-boot:run \```
### With Docker
run blow commands in order:
```git clone https://gitlab.com/mehran.mehrafarin/open-shop.git```
```cd open-shop```
```mvn clean package -DskipTests```
```docker build -t open-shop:v-01 .``` (dot at the end of the command is important)
```docker run -d --network=host  --name=open-shop open-shop:v-01```
## Get started
First of all you should initialize the database.
There is a database.sql file in database/database.sql you have to import it.

There is a Postman collection ```open-shop.postman_collection.json``` in the root of the project for all possible requests.
Now you can sign-in through this url: [localhost:8080/api/sign-in](localhost:8080/api/sign-in "open-shop application"): with ```username:admin``` and ```password:admin``` it returns token in the header of the response.
In order to use application you should put it in the header like this "Authorization":"Bearer {token}"

Please report issues and bugs in the Gitlab issue tracker.
## Settings

Set the following environment variables in order to change the application settings:

Field name | Default value | Description | 
--- | --- | --- | 
SERVER_PORT | 8080 | Application port |
JWT Secret Key | secret | secret key for generating token
## Changelog
18.09.2021 initial release 1.0