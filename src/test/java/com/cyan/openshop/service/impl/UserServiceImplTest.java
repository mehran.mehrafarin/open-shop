package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Role;
import com.cyan.openshop.entity.User;
import com.cyan.openshop.repository.RoleRepository;
import com.cyan.openshop.repository.UserRepository;
import com.cyan.openshop.service.UserService;
import com.cyan.openshop.web.model.UserModel;
import com.cyan.openshop.web.response_model.ProductResponseModel;
import com.cyan.openshop.web.response_model.UserResponseModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository repository;

    @Mock
    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    private ModelMapper modelMapper;

    private UserService service;

    private Role role;

    private User entity;

    @BeforeEach
    public void setup() {
        passwordEncoder = new BCryptPasswordEncoder();
        modelMapper = new ModelMapper();

        role = new Role();
        role.setId(1L);
        role.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");
        role.setName("ROLE_USER");

        Collection<Role> roles = new ArrayList<>();
        roles.add(role);

        entity = new User();
        entity.setId(1L);
        entity.setRoles(roles);
        entity.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");

        service = new UserServiceImpl(repository, roleRepository, passwordEncoder, modelMapper);
    }

    @Test
    void getByCode() {
        String actualResult = "0ae53188-8012-47c8-ab05-d6a7075c6686";
        Mockito.when(repository.findByCode("0ae53188-8012-47c8-ab05-d6a7075c6686")).thenReturn(Optional.of(entity));

        UserResponseModel response = service.getByCode("0ae53188-8012-47c8-ab05-d6a7075c6686");

        assertEquals(response.getModel().getCode(), actualResult);
    }

    @Test
    void add() {
        Mockito.when(repository.findByUsername("admin")).thenReturn(Optional.empty());
        Mockito.when(roleRepository.findByName("ROLE_USER")).thenReturn(role);

        UserModel userModel = new UserModel();
        userModel.setUsername("admin");
        userModel.setPassword("admin");

        UserResponseModel response = service.add(userModel);

        assertNotNull(response.getModel());
    }
}