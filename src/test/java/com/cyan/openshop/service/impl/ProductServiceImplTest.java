package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Category;
import com.cyan.openshop.entity.Product;
import com.cyan.openshop.repository.CategoryRepository;
import com.cyan.openshop.repository.ProductRepository;
import com.cyan.openshop.service.ProductService;
import com.cyan.openshop.web.model.ProductModel;
import com.cyan.openshop.web.response_model.ProductResponseModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository repository;

    @Mock
    private CategoryRepository categoryRepository;

    private ProductService service;

    private ModelMapper modelMapper;

    private Category category;

    private Product entity;

    @BeforeEach
    public void setup() {
        modelMapper = new ModelMapper();
        category = new Category();
        category.setId(1L);
        category.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");
        category.setName("Electronic");
        category.setDescription("This is electronic category");
        category.setState("enable");

        entity = new Product();
        entity.setId(1L);
        entity.setCategory(category);
        entity.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");
        entity.setName("Laptop");
        entity.setPrice(new BigDecimal("1000"));
        entity.setState("enable");

        service = new ProductServiceImpl(repository, categoryRepository, modelMapper);
    }


    @Test
    void getByCode() {
        String actualResult = "0ae53188-8012-47c8-ab05-d6a7075c6686";
        Mockito.when(repository.findByCode("0ae53188-8012-47c8-ab05-d6a7075c6686")).thenReturn(Optional.of(entity));

        ProductResponseModel response = service.getByCode("0ae53188-8012-47c8-ab05-d6a7075c6686");

        assertEquals(response.getModel().getCode(), actualResult);
    }

    @Test
    void add() {

        Mockito.when(categoryRepository.findByCode("0ae53188-8012-47c8-ab05-d6a7075c6686")).thenReturn(Optional.of(category));
        ProductModel productModel = new ProductModel();
        productModel.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");
        productModel.setCategoryCode("0ae53188-8012-47c8-ab05-d6a7075c6686");
        productModel.setName("Laptop");
        productModel.setPrice(new BigDecimal("1000"));
        productModel.setState("enable");

        ProductResponseModel response =  service.add(productModel);
        assertNotNull(response.getModel().getCode());
    }

}