package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Comment;
import com.cyan.openshop.entity.Role;
import com.cyan.openshop.entity.User;
import com.cyan.openshop.repository.CommentRepository;
import com.cyan.openshop.repository.ProductRepository;
import com.cyan.openshop.repository.RateRepository;
import com.cyan.openshop.repository.UserRepository;
import com.cyan.openshop.service.CommentService;
import com.cyan.openshop.service.UserService;
import com.cyan.openshop.web.response_model.CommentResponseModel;
import com.cyan.openshop.web.response_model.UserResponseModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CommentServiceImplTest {

    @Mock
    private CommentRepository repository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ProductRepository productRepository;

    @Mock
    private RateRepository rateRepository;

    @Mock
    private UserService userService;


    private ModelMapper modelMapper;

    private Comment entity;

    private CommentService service;

    @BeforeEach
    public void setup() {
        modelMapper = new ModelMapper();

        entity = new Comment();
        entity.setId(1L);
        entity.setCode("0ae53188-8012-47c8-ab05-d6a7075c6686");

        service = new CommentServiceImpl(repository, userRepository, productRepository, rateRepository, userService, modelMapper);
    }

    @Test
    void getByCode() {

        String actualResult = "0ae53188-8012-47c8-ab05-d6a7075c6686";
        Mockito.when(repository.findByCode("0ae53188-8012-47c8-ab05-d6a7075c6686")).thenReturn(Optional.of(entity));

        CommentResponseModel response = service.getByCode("0ae53188-8012-47c8-ab05-d6a7075c6686");

        assertEquals(response.getModel().getCode(), actualResult);
    }
}