package com.cyan.openshop.repository;


import com.cyan.openshop.entity.Rate;
import com.cyan.openshop.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link Rate} entity.
 */
@Repository
public interface RateRepository extends JpaRepository<Rate, Long> {

    Optional<Rate> findByCode(String code);

    void deleteByCode(String code);

    Optional<Rate> findByUser(@Param("user") User user);

}
