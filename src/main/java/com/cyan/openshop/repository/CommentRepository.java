package com.cyan.openshop.repository;


import com.cyan.openshop.entity.Comment;
import com.cyan.openshop.entity.Product;
import com.cyan.openshop.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link Comment} entity.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    Optional<Comment> findByCode(String code);

    @Query("SELECT e FROM Comment e WHERE (:user IS NULL OR e.user = :user) AND (:product IS NULL OR e.product = :product) ")
    Page<Comment> findAllByUserAndProduct(@Param("user") User user, @Param("product") Product product, Pageable pageable);

    void deleteByCode(String code);

}
