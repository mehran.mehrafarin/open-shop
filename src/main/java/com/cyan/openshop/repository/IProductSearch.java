package com.cyan.openshop.repository;

import java.math.BigDecimal;

public interface IProductSearch {

    Long getId();
    String getCode();
    String getName();
    BigDecimal getPrice();
    String getState();
    String getRate();

}
