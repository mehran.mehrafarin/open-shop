package com.cyan.openshop.repository;


import com.cyan.openshop.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link Product} entity.
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Optional<Product> findByCode(String code);

    void deleteByCode(String code);

    @Query("SELECT e.id AS id, e.code AS code, e.name AS name, e.price AS price, e.state AS state, AVG(r.value) AS rate FROM Product e LEFT JOIN Rate r ON r.product = e WHERE " +
            "(:name IS NULL OR e.name LIKE %:name%) AND " +
            "(:maxPrice IS NULL OR e.price <= :maxPrice) AND " +
            "(:minPrice IS NULL OR e.price >= :minPrice) " +
            "GROUP BY e.id HAVING (:maxRate IS NULL OR AVG(r.value) <= :maxRate) AND (:minRate  IS NULL OR AVG(r.value) >= :minRate) ")
    List<IProductSearch> findAllByNameAndMinPriceAndMaxPriceAndMinRateAndMaxRateWithQuery(@Param("name") String name, @Param("minPrice") BigDecimal minPrice, @Param("maxPrice") BigDecimal maxPrice, @Param("minRate") Double minRate, @Param("maxRate") Double maxRate, Pageable pageable);

}
