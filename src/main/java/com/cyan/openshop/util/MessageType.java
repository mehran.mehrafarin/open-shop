package com.cyan.openshop.util;

public enum MessageType {

    SUCCESS,
    INFO,
    WARNING,
    ERROR
}
