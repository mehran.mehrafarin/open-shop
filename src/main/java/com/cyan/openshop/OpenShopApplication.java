package com.cyan.openshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class OpenShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenShopApplication.class, args);
    }

}
