package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Comment;
import com.cyan.openshop.entity.Product;
import com.cyan.openshop.entity.Rate;
import com.cyan.openshop.entity.User;
import com.cyan.openshop.exception.ResourceNotFoundException;
import com.cyan.openshop.repository.CommentRepository;
import com.cyan.openshop.repository.ProductRepository;
import com.cyan.openshop.repository.RateRepository;
import com.cyan.openshop.repository.UserRepository;
import com.cyan.openshop.service.CommentService;
import com.cyan.openshop.service.UserService;
import com.cyan.openshop.web.model.CommentModel;
import com.cyan.openshop.web.request_model.CommentRequestModel;
import com.cyan.openshop.web.response_model.CommentResponseModel;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.cyan.openshop.util.UUIDGenerator.getUUID;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {


    private CommentRepository repository;

    private UserRepository userRepository;

    private ProductRepository productRepository;

    private RateRepository rateRepository;

    private UserService userService;

    private ModelMapper modelMapper;

    public CommentServiceImpl(CommentRepository repository, UserRepository userRepository, ProductRepository productRepository, RateRepository rateRepository, UserService userService, ModelMapper modelMapper) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.rateRepository = rateRepository;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @Override
    public CommentResponseModel getByCode(String code) {

        Comment entity = repository.findByCode(code).orElseThrow(() -> new ResourceNotFoundException("Comment not found."));

        CommentModel model = modelMapper.map(entity, CommentModel.class);

        CommentResponseModel response = new CommentResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public CommentResponseModel getByUserOrProduct(CommentRequestModel request) {

        Product product = null;

        if (request.getProductCode() != null)
            product = productRepository.findByCode(request.getProductCode()).orElseThrow(() -> new ResourceNotFoundException("Product not found."));

        User user = null;

        if (request.getUserCode() != null)
            user = userRepository.findByCode(request.getUserCode()).orElseThrow(() -> new ResourceNotFoundException("User not found."));

        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by("id").descending());

        Page<Comment> page = repository.findAllByUserAndProduct(user, product, pageable);

        List<CommentModel> models = page.getContent().stream().map(e -> modelMapper.map(e, CommentModel.class)).collect(Collectors.toList());

        CommentResponseModel response = new CommentResponseModel();
        response.setModels(models);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public CommentResponseModel add(CommentModel model) {

        Product product = productRepository.findByCode(model.getProductCode()).orElseThrow(() -> new ResourceNotFoundException("Product not found."));
        User user = userService.getCurrentUser();

        if (model.getRateValue() != null) {

            Optional<Rate> rateOptional = rateRepository.findByUser(user);

            if (rateOptional.isPresent()) {
                rateOptional.get().setValue(model.getRateValue());

            } else {
                Rate rate = new Rate();
                rate.setUser(user);
                rate.setProduct(product);
                rate.setCode(getUUID());
                rate.setState("enable");
                rate.setValue(model.getRateValue());
                rateRepository.save(rate);
            }
        }

        Comment entity = new Comment();
        entity.setUser(user);
        entity.setProduct(product);
        entity.setCode(getUUID());
        entity.setTitle(model.getTitle());
        entity.setDescription(model.getDescription());
        entity.setState("enable");

        repository.save(entity);

        model.setCode(entity.getCode());

        CommentResponseModel response = new CommentResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.CREATED);
        response.setMessage("Comment added successfully.");

        return response;
    }

    @Override
    public CommentResponseModel remove(String code) {

        repository.deleteByCode(code);

        CommentResponseModel response = new CommentResponseModel();
        response.setHttpStatus(HttpStatus.OK);
        response.setMessage("Comment removed successfully.");

        return response;
    }
}
