package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Category;
import com.cyan.openshop.entity.Product;
import com.cyan.openshop.exception.ResourceNotFoundException;
import com.cyan.openshop.repository.CategoryRepository;
import com.cyan.openshop.repository.IProductSearch;
import com.cyan.openshop.repository.ProductRepository;
import com.cyan.openshop.service.ProductService;
import com.cyan.openshop.web.model.ProductModel;
import com.cyan.openshop.web.request_model.ProductRequestModel;
import com.cyan.openshop.web.response_model.ProductResponseModel;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.cyan.openshop.util.UUIDGenerator.getUUID;


@Service
@Transactional
public class ProductServiceImpl implements ProductService {


    private ProductRepository repository;

    private CategoryRepository categoryRepository;

    private ModelMapper modelMapper;


    public ProductServiceImpl(ProductRepository repository, CategoryRepository categoryRepository, ModelMapper modelMapper) {
        this.repository = repository;
        this.categoryRepository = categoryRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public ProductResponseModel getByCode(String code) {

        Product entity = repository.findByCode(code).orElseThrow(() -> new ResourceNotFoundException("Product not found."));

        ProductModel model = modelMapper.map(entity, ProductModel.class);

        ProductResponseModel response = new ProductResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public ProductResponseModel getPaginated(ProductRequestModel request) {

        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by("id").descending());

        List<IProductSearch> entities = repository.findAllByNameAndMinPriceAndMaxPriceAndMinRateAndMaxRateWithQuery(request.getName() , request.getMinPrice() ,request.getMaxPrice() , request.getMinRate(), request.getMaxRate() , pageable);

        List<ProductModel> models = entities.stream().map(e -> modelMapper.map(e, ProductModel.class)).collect(Collectors.toList());

        ProductResponseModel response = new ProductResponseModel();
        response.setModels(models);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public ProductResponseModel add(ProductModel model) {

        Category category = categoryRepository.findByCode(model.getCategoryCode()).orElseThrow(() -> new ResourceNotFoundException("Category not found."));

        Product entity = new Product();
        entity.setCategory(category);
        entity.setCode(getUUID());
        entity.setName(model.getName());
        entity.setPrice(model.getPrice());
        entity.setState(model.getState());


        repository.save(entity);

        model.setCode(entity.getCode());

        ProductResponseModel response = new ProductResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.CREATED);
        response.setMessage("Product added successfully.");

        return response;
    }

    @Override
    public ProductResponseModel edit(ProductModel model) {

        Product entity = repository.findByCode(model.getCode()).orElseThrow(() -> new ResourceNotFoundException("Product not found."));
        entity.setName(model.getName());
        entity.setPrice(model.getPrice());
        entity.setState(model.getState());


        if (model.getCategoryCode() != null && model.getCategoryCode().trim().length() != 0) {

            Category category = categoryRepository.findByCode(model.getCategoryCode()).orElseThrow(() -> new ResourceNotFoundException("Category not found."));
            entity.setCategory(category);
        }


        repository.save(entity);

        ProductResponseModel response = new ProductResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.OK);
        response.setMessage("Product edited successfully.");

        return response;
    }

    @Override
    public ProductResponseModel remove(String code) {

        repository.deleteByCode(code);

        ProductResponseModel response = new ProductResponseModel();
        response.setHttpStatus(HttpStatus.OK);
        response.setMessage("Product removed successfully.");

        return response;
    }
}
