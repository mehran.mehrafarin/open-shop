package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Role;
import com.cyan.openshop.repository.RoleRepository;
import com.cyan.openshop.service.RoleService;
import com.cyan.openshop.web.model.RoleModel;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

import static com.cyan.openshop.util.UUIDGenerator.getUUID;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    private RoleRepository repository;

    private ModelMapper modelMapper;

    public RoleServiceImpl(RoleRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Override
    public RoleModel add(RoleModel model) {

        Role entity = modelMapper.map(model , Role.class);
        entity.setCode(getUUID());
        entity.setCreatedDate(LocalDate.now());

        repository.save(entity);

        return null;
    }
}
