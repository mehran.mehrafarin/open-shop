package com.cyan.openshop.service.impl;

import com.cyan.openshop.entity.Category;
import com.cyan.openshop.exception.ResourceNotFoundException;
import com.cyan.openshop.repository.CategoryRepository;
import com.cyan.openshop.service.CategoryService;
import com.cyan.openshop.shared.model.GenericRequestModel;
import com.cyan.openshop.web.model.CategoryModel;
import com.cyan.openshop.web.response_model.CategoryResponseModel;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.cyan.openshop.util.UUIDGenerator.getUUID;


@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository repository;

    private ModelMapper modelMapper;


    public CategoryServiceImpl(CategoryRepository repository, ModelMapper modelMapper) {
        this.repository = repository;
        this.modelMapper = modelMapper;
    }

    @Override
    public CategoryResponseModel getByCode(String code) {

        Category entity = repository.findByCode(code).orElseThrow(() -> new ResourceNotFoundException("Category not found."));

        CategoryModel model = modelMapper.map(entity, CategoryModel.class);

        CategoryResponseModel response = new CategoryResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public CategoryResponseModel getPaginated(GenericRequestModel request) {

        Pageable pageable = PageRequest.of(request.getPage(), request.getSize(), Sort.by("id").descending());

        Page<Category> page = repository.findAll(pageable);

        List<CategoryModel> models = page.getContent().stream().map(e -> modelMapper.map(e, CategoryModel.class)).collect(Collectors.toList());

        CategoryResponseModel response = new CategoryResponseModel();
        response.setModels(models);
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    public CategoryResponseModel add(CategoryModel model) {

        Category entity = modelMapper.map(model, Category.class);
        entity.setCode(getUUID());

        repository.save(entity);

        model.setCode(entity.getCode());

        CategoryResponseModel response = new CategoryResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.CREATED);
        response.setMessage("Category added successfully.");

        return response;
    }

    @Override
    public CategoryResponseModel edit(CategoryModel model) {

        Category entity = repository.findByCode(model.getCode()).orElseThrow(() -> new ResourceNotFoundException("Category not found."));

        entity = modelMapper.map(model, Category.class);

        repository.save(entity);

        CategoryResponseModel response = new CategoryResponseModel();
        response.setModel(model);
        response.setHttpStatus(HttpStatus.OK);
        response.setMessage("Category edited successfully.");

        return response;
    }

    @Override
    public CategoryResponseModel remove(String code) {

        repository.deleteByCode(code);

        CategoryResponseModel response = new CategoryResponseModel();
        response.setHttpStatus(HttpStatus.OK);
        response.setMessage("Category removed successfully.");

        return response;
    }
}
