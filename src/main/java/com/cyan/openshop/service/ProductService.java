package com.cyan.openshop.service;

import com.cyan.openshop.web.model.ProductModel;
import com.cyan.openshop.web.request_model.ProductRequestModel;
import com.cyan.openshop.web.response_model.ProductResponseModel;

public interface ProductService {

    ProductResponseModel getByCode(String code);

    ProductResponseModel getPaginated(ProductRequestModel model);

    ProductResponseModel add(ProductModel model);

    ProductResponseModel edit(ProductModel model);

    ProductResponseModel remove(String code);
}
