package com.cyan.openshop.service;

import com.cyan.openshop.entity.User;
import com.cyan.openshop.web.model.RoleModel;
import com.cyan.openshop.web.model.UserModel;
import com.cyan.openshop.web.request_model.UserRequestModel;
import com.cyan.openshop.web.response_model.UserResponseModel;

public interface UserService {

    User getCurrentUser();

    UserResponseModel getByCode(String code);

    UserResponseModel getPaginated(UserRequestModel model);

    UserResponseModel add(UserModel model);

    UserResponseModel signUp(UserModel model);

    UserResponseModel edit(UserModel model);

    UserResponseModel remove(String code);

}
