package com.cyan.openshop.service;

import com.cyan.openshop.web.model.CommentModel;
import com.cyan.openshop.web.request_model.CommentRequestModel;
import com.cyan.openshop.web.response_model.CommentResponseModel;

public interface CommentService {

    CommentResponseModel getByCode(String code);

    CommentResponseModel getByUserOrProduct(CommentRequestModel request);

    CommentResponseModel add(CommentModel model);

    CommentResponseModel remove(String code);
}
