package com.cyan.openshop.service;

import com.cyan.openshop.web.model.RoleModel;
import com.cyan.openshop.web.response_model.UserResponseModel;

public interface RoleService {

    RoleModel add(RoleModel model);

}
