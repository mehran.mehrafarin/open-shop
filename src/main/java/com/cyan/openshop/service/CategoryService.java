package com.cyan.openshop.service;

import com.cyan.openshop.shared.model.GenericRequestModel;
import com.cyan.openshop.web.model.CategoryModel;
import com.cyan.openshop.web.response_model.CategoryResponseModel;

public interface CategoryService {

    CategoryResponseModel getByCode(String code);

    CategoryResponseModel getPaginated(GenericRequestModel model);

    CategoryResponseModel add(CategoryModel model);

    CategoryResponseModel edit(CategoryModel model);

    CategoryResponseModel remove(String code);
}
