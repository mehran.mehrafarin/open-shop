package com.cyan.openshop.exception;

public class SignUpException extends RuntimeException {

    public SignUpException(String message) {
        super(message);
    }
}
