package com.cyan.openshop.shared.model;


public class GenericRequestModel {

    private Integer page;

    private Integer size;

    private String code;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if (page < 0) page = 0;
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        if (size < 0) size = 0;
        this.size = size;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
