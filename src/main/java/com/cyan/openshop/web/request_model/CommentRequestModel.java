package com.cyan.openshop.web.request_model;

import com.cyan.openshop.shared.model.GenericRequestModel;

public class CommentRequestModel extends GenericRequestModel {

    private String productCode;

    private String userCode;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
