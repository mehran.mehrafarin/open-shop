package com.cyan.openshop.web.request_model;

import com.cyan.openshop.shared.model.GenericRequestModel;

import java.math.BigDecimal;

public class ProductRequestModel extends GenericRequestModel {

    private String name;

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    private Double minRate;

    private Double maxRate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMinRate() {
        return minRate;
    }

    public void setMinRate(Double minRate) {
        this.minRate = minRate;
    }

    public Double getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(Double maxRate) {
        this.maxRate = maxRate;
    }

}
