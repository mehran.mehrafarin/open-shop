package com.cyan.openshop.web.response_model;

import com.cyan.openshop.web.model.CommentModel;
import org.springframework.http.HttpStatus;

import java.util.List;

public class CommentResponseModel {

    private HttpStatus httpStatus;

    private String message;

    private CommentModel model;

    private List<CommentModel> models;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CommentModel getModel() {
        return model;
    }

    public void setModel(CommentModel model) {
        this.model = model;
    }

    public List<CommentModel> getModels() {
        return models;
    }

    public void setModels(List<CommentModel> models) {
        this.models = models;
    }
}
