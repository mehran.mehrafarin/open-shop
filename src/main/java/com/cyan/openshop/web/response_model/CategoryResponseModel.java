package com.cyan.openshop.web.response_model;

import com.cyan.openshop.web.model.CategoryModel;
import org.springframework.http.HttpStatus;

import java.util.List;

public class CategoryResponseModel {

    private HttpStatus httpStatus;

    private String message;

    private CategoryModel model;

    private List<CategoryModel> models;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CategoryModel getModel() {
        return model;
    }

    public void setModel(CategoryModel model) {
        this.model = model;
    }

    public List<CategoryModel> getModels() {
        return models;
    }

    public void setModels(List<CategoryModel> models) {
        this.models = models;
    }
}
