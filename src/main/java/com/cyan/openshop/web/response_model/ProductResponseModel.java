package com.cyan.openshop.web.response_model;

import com.cyan.openshop.web.model.ProductModel;
import com.cyan.openshop.web.model.UserModel;
import org.springframework.http.HttpStatus;

import java.util.List;

public class ProductResponseModel {

    private HttpStatus httpStatus;

    private String message;

    private ProductModel model;

    private List<ProductModel> models;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductModel getModel() {
        return model;
    }

    public void setModel(ProductModel model) {
        this.model = model;
    }

    public List<ProductModel> getModels() {
        return models;
    }

    public void setModels(List<ProductModel> models) {
        this.models = models;
    }
}
