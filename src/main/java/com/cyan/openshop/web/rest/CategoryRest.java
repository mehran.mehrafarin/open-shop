package com.cyan.openshop.web.rest;

import com.cyan.openshop.service.CategoryService;
import com.cyan.openshop.shared.model.GenericRequestModel;
import com.cyan.openshop.web.model.CategoryModel;
import com.cyan.openshop.web.response_model.CategoryResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/categories")
public class CategoryRest {


    private CategoryService service;

    @Autowired
    public CategoryRest(CategoryService service) {
        this.service = service;
    }

    @GetMapping("/{code}")
    public ResponseEntity<CategoryResponseModel> getByCode(@PathVariable String code) {
        CategoryResponseModel response = service.getByCode(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @GetMapping("/pagination")
    public ResponseEntity<CategoryResponseModel> getPaginated(GenericRequestModel model) {
        CategoryResponseModel response = service.getPaginated(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PostMapping
    public ResponseEntity<CategoryResponseModel> add(@Valid @RequestBody CategoryModel model) {
        CategoryResponseModel response = service.add(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PutMapping
    public ResponseEntity<CategoryResponseModel> edit(@Valid @RequestBody CategoryModel model) {
        CategoryResponseModel response = service.edit(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<CategoryResponseModel> remove(@PathVariable String code) {
        CategoryResponseModel response = service.remove(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

}
