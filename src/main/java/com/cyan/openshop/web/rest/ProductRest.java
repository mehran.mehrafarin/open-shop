package com.cyan.openshop.web.rest;

import com.cyan.openshop.service.ProductService;
import com.cyan.openshop.shared.model.GenericRequestModel;
import com.cyan.openshop.web.model.ProductModel;
import com.cyan.openshop.web.request_model.ProductRequestModel;
import com.cyan.openshop.web.response_model.ProductResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/products")
public class ProductRest {


    private ProductService service;

    @Autowired
    public ProductRest(ProductService service) {
        this.service = service;
    }

    @GetMapping("/{code}")
    public ResponseEntity<ProductResponseModel> getByCode(@PathVariable String code) {
        ProductResponseModel response = service.getByCode(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @GetMapping("/pagination")
    public ResponseEntity<ProductResponseModel> getPaginated(@Valid ProductRequestModel model) {
        ProductResponseModel response = service.getPaginated(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PostMapping
    public ResponseEntity<ProductResponseModel> add(@Valid @RequestBody ProductModel model) {
        ProductResponseModel response = service.add(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PutMapping
    public ResponseEntity<ProductResponseModel> edit(@Valid @RequestBody ProductModel model) {
        ProductResponseModel response = service.edit(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<ProductResponseModel> remove(@PathVariable String code) {
        ProductResponseModel response = service.remove(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

}
