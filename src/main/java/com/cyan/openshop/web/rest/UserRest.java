package com.cyan.openshop.web.rest;

import com.cyan.openshop.service.UserService;
import com.cyan.openshop.web.model.UserModel;
import com.cyan.openshop.web.request_model.UserRequestModel;
import com.cyan.openshop.web.response_model.UserResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserRest {

    private UserService service;

    @Autowired
    public UserRest(UserService service) {
        this.service = service;
    }


    @GetMapping("/{code}")
    public ResponseEntity<UserResponseModel> getByCode(@PathVariable String code) {
        UserResponseModel response = service.getByCode(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @GetMapping("/pagination")
    public ResponseEntity<UserResponseModel> getPaginated(UserRequestModel model) {
        UserResponseModel response = service.getPaginated(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PostMapping
    public ResponseEntity<UserResponseModel> add(@RequestBody @Valid UserModel model) {
        UserResponseModel response = service.add(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PutMapping
    public ResponseEntity<UserResponseModel> edit(@RequestBody @Valid UserModel model) {
        UserResponseModel response = service.edit(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @DeleteMapping("/{code}")
    public ResponseEntity<UserResponseModel> remove(@PathVariable String code) {
        UserResponseModel response = service.remove(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

}
