package com.cyan.openshop.web.rest;

import com.cyan.openshop.service.CommentService;
import com.cyan.openshop.shared.model.GenericRequestModel;
import com.cyan.openshop.web.model.CommentModel;
import com.cyan.openshop.web.request_model.CommentRequestModel;
import com.cyan.openshop.web.response_model.CommentResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/comments")
public class CommentRest {


    private CommentService service;


    public CommentRest(CommentService service) {
        this.service = service;
    }

    @GetMapping("/{code}")
    public ResponseEntity<CommentResponseModel> getByCode(@PathVariable String code) {
        CommentResponseModel response = service.getByCode(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @GetMapping("/pagination")
    public ResponseEntity<CommentResponseModel> getPaginated(CommentRequestModel model) {
        CommentResponseModel response = service.getByUserOrProduct(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

    @PostMapping
    public ResponseEntity<CommentResponseModel> add(@Valid @RequestBody CommentModel model) {
        CommentResponseModel response = service.add(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }


    @DeleteMapping("/{code}")
    public ResponseEntity<CommentResponseModel> remove(@PathVariable String code) {
        CommentResponseModel response = service.remove(code);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }

}
