package com.cyan.openshop.web.rest;


import com.cyan.openshop.service.UserService;
import com.cyan.openshop.web.model.UserModel;
import com.cyan.openshop.web.response_model.UserResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class JWTAuthenticationRest {

    private UserService service;

    @Autowired
    public JWTAuthenticationRest(UserService service) {
        this.service = service;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<UserResponseModel> signUp(@Valid @RequestBody UserModel model) {
        UserResponseModel response = service.add(model);
        return ResponseEntity.status(response.getHttpStatus()).body(response);
    }
}
